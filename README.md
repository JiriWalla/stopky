**Projekt - stopky**
-

Funkce
-
Při spuštěném čítači TIM2, který je nastavený na 10 milisekund se po každém přetečení přičte jednička do proměnné, udávající aktuální hodnotu času. Pomocí tlačítek, přiřazených k externímu přerušení zastavujeme a spouštíme TIM2, provádíme nulování aktuální hodnoty času a mezičasu. Dále pomocí externích přerušení od příslušných tlačítek nastavujeme hodnotu v jakém režimu se stopky nachází. Hlavní program v inicializaci nastaví čítač TIM2 na 10 ms a externí přerušení na sestupnou hranu. Hlavní smyčka programu pouze provádí zobrazení příslušné hodnoty dle módu stopek na displej, který je provozován v dymamickém režimu(Při jednom průchodu programem je zobrazená pouze jedna pozice na displeji.). Vlastní převod z aktualní hodnoty času nebo mezičasu je proveden jednou za šest průchodů hlavní smyčky programu, vždy na začátku zobrazované hodnoty. Na konci cyklu je nastaveno zpoždění, které určuje délku svitu jedné pozice. 

Tabulka použitých součástek
-
| Součástky |Počet| Hodnoty | Typ |
|:---:|:---:|:---:|:---:|
|Rezistor| 8 | 390 Ω | - |
|Rezistor | 7 | 1000 Ω |-|
| Tlačítko | 3 | - | - |
|7 - segment | 6 | - | LA5621-11|
| Dekodér | 1 | - | E147D |
| Transistor | 6 | - | C557B |
| Transistor | 1 | - | C547A | 

Blokové schéma
-
```mermaid
graph TD;
  STM8-->7segment;
  STM8-->dekodér(E147D);
  dekodér(E147D)-->7segment;
  tlačítka -->STM8
```

Nucleo 8S208RB
-
Desky STM8 Nucleo-64 poskytují uživatelům cenově dostupný a flexibilní způsob, jak vyzkoušet nové koncepty a stavět prototypy s mikrokontrolérem STM8 zabaleným v LQFP64, který poskytuje různé kombinace výkonu, spotřeby energie a funkcí.

![alt text](img/stm.webp)

Fotka zapojení
-
![alt text](img/IMG_0262.JPG)


Závěr
-
Práce na projektu pro mě byla výzvou. První týdny mi program vůbec nefungoval, protože STM se mnou vůbec nechtělo komunikovat. Proto jsem si musel nainstalovat jiné vývojové prostředí, kde jsem také projekt realizoval. Po několika neůspěšných pokusech se mi povedlo nakonec povedlo stopky uvést do provozu. I přes všechny tyto komplikace mě práce na projektu celkem bavila. 

Autor
-
Jiří Walla
 
