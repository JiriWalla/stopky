#include "stm8s.h"
#include "stm8s_it.h"

#define TECKA_POS 3
#define TECKA_BIT 0x80


#define smSTOP  1
#define smSPLIT 2
#define smRUN 3

char stopkymode;

int32_t cas=0;
int32_t mezicas=0;

INTERRUPT_HANDLER(EXTI_PORTE_IRQHandler, 7)
{
	if(GPIO_ReadInputPin(GPIOE, GPIO_PIN_4) == RESET)  //tlacitko START
	{
		if(stopkymode==smSTOP)
		{
			TIM2_SetCounter(0x0000);
			TIM2_Cmd(ENABLE);
			GPIO_WriteHigh(GPIOC, GPIO_PIN_5);
		}
		stopkymode = smRUN;
	}
	if(GPIO_ReadInputPin(GPIOE, GPIO_PIN_3) == RESET)	//tlacitko STOP
	{
		TIM2_Cmd(DISABLE);
		GPIO_WriteLow(GPIOC, GPIO_PIN_5);
		if(stopkymode == smSTOP)
		{
			cas=0;
			mezicas=0;
		}
		stopkymode = smSTOP;
	}	
	if(GPIO_ReadInputPin(GPIOE, GPIO_PIN_0) == RESET) //tlacitko MEZICAS
	{
		if (stopkymode != smSTOP)
		{
			mezicas=cas;
			stopkymode = smSPLIT;
		}
	}	
	

}

INTERRUPT_HANDLER(TIM2_UPD_OVF_BRK_IRQHandler, 13)
{
	cas++;
	if (cas > 359999) {cas = 0;}
	TIM2_ClearITPendingBit(TIM2_IT_UPDATE);
}

 
void main(void)
{
    int dZobrCas;
    int PomCas;
    char PoleCas[6];
	char zobrmisto=0;
    char setin = 0;
    char sek = 0;
    char min = 0;
		char i;

    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);
		GPIO_Init(GPIOC, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_SLOW);

		GPIO_Init(GPIOD, GPIO_PIN_ALL, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOB, GPIO_PIN_ALL, GPIO_MODE_OUT_PP_HIGH_SLOW);
    
		GPIO_Init(GPIOE, GPIO_PIN_0, GPIO_MODE_IN_PU_IT);
    GPIO_Init(GPIOE, GPIO_PIN_3, GPIO_MODE_IN_PU_IT);
		GPIO_Init(GPIOE, GPIO_PIN_4, GPIO_MODE_IN_PU_IT);
		EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOE, EXTI_SENSITIVITY_FALL_ONLY);
	
		
		TIM2_TimeBaseInit(TIM2_PRESCALER_128, 1249); //Nastaveno na 10 ms
		TIM2_ITConfig(TIM2_IT_UPDATE, ENABLE);   //
		
		
		
		TIM2_Cmd(DISABLE);
		
		stopkymode = smSTOP;
		
		enableInterrupts();
      
    while (1)
    {
			GPIO_Write(GPIOB, 0xFF);
			if (zobrmisto > 5)
			{
				if (stopkymode == smSPLIT)
					{dZobrCas=mezicas;}
				else
					{dZobrCas=cas;}               //P�epo�et
				setin= dZobrCas% 100;
				PomCas = dZobrCas / 100;
				sek = PomCas % 60;
				min = PomCas / 60;
	
				PoleCas[5] = setin % 10;
				PoleCas[4] = setin / 10;
				PoleCas[3] = sek % 10;
				PoleCas[2] = sek / 10;
				PoleCas[1] = min % 10;
				PoleCas[0] = min / 10;
				zobrmisto=0;
				
			}
			if (zobrmisto == TECKA_POS)
				{ GPIO_Write(GPIOD, (PoleCas[zobrmisto] | TECKA_BIT));}
      		else
				{GPIO_Write(GPIOD, PoleCas[zobrmisto]);}
			
			GPIO_Write(GPIOB, ~(1<<zobrmisto));
			
    
      zobrmisto++;
			for( i=0;i < 200;i++);
	}
    
}
