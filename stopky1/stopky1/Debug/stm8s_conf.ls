   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.12.6 - 16 Dec 2021
   3                     ; Generator (Limited) V4.5.4 - 16 Dec 2021
  63                     ; 12 void assert_failed(uint8_t* file, uint32_t line)
  63                     ; 13 {
  65                     	switch	.text
  66  0000               _assert_failed:
  68  0000 89            	pushw	x
  69       00000000      OFST:	set	0
  72                     ; 14   (void)file;
  74                     ; 15   (void)line;
  76  0001               L33:
  78  0001 20fe          	jra	L33
  91                     	xdef	_assert_failed
 110                     	end
