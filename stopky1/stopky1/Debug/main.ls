   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.12.6 - 16 Dec 2021
   3                     ; Generator (Limited) V4.5.4 - 16 Dec 2021
  14                     	bsct
  15  0000               _cas:
  16  0000 00000000      	dc.l	0
  17  0004               _mezicas:
  18  0004 00000000      	dc.l	0
  56                     ; 17 INTERRUPT_HANDLER(EXTI_PORTE_IRQHandler, 7)
  56                     ; 18 {
  57                     	switch	.text
  58  0000               f_EXTI_PORTE_IRQHandler:
  60  0000 8a            	push	cc
  61  0001 84            	pop	a
  62  0002 a4bf          	and	a,#191
  63  0004 88            	push	a
  64  0005 86            	pop	cc
  65  0006 3b0002        	push	c_x+2
  66  0009 be00          	ldw	x,c_x
  67  000b 89            	pushw	x
  68  000c 3b0002        	push	c_y+2
  69  000f be00          	ldw	x,c_y
  70  0011 89            	pushw	x
  73                     ; 19 	if(GPIO_ReadInputPin(GPIOE, GPIO_PIN_4) == RESET)  //tlacitko START
  75  0012 4b10          	push	#16
  76  0014 ae5014        	ldw	x,#20500
  77  0017 cd0000        	call	_GPIO_ReadInputPin
  79  001a 5b01          	addw	sp,#1
  80  001c 4d            	tnz	a
  81  001d 261c          	jrne	L12
  82                     ; 21 		if(stopkymode==smSTOP)
  84  001f b600          	ld	a,_stopkymode
  85  0021 a101          	cp	a,#1
  86  0023 2612          	jrne	L32
  87                     ; 23 			TIM2_SetCounter(0x0000);
  89  0025 5f            	clrw	x
  90  0026 cd0000        	call	_TIM2_SetCounter
  92                     ; 24 			TIM2_Cmd(ENABLE);
  94  0029 a601          	ld	a,#1
  95  002b cd0000        	call	_TIM2_Cmd
  97                     ; 25 			GPIO_WriteHigh(GPIOC, GPIO_PIN_5);
  99  002e 4b20          	push	#32
 100  0030 ae500a        	ldw	x,#20490
 101  0033 cd0000        	call	_GPIO_WriteHigh
 103  0036 84            	pop	a
 104  0037               L32:
 105                     ; 27 		stopkymode = smRUN;
 107  0037 35030000      	mov	_stopkymode,#3
 108  003b               L12:
 109                     ; 29 	if(GPIO_ReadInputPin(GPIOE, GPIO_PIN_3) == RESET)	//tlacitko
 111  003b 4b08          	push	#8
 112  003d ae5014        	ldw	x,#20500
 113  0040 cd0000        	call	_GPIO_ReadInputPin
 115  0043 5b01          	addw	sp,#1
 116  0045 4d            	tnz	a
 117  0046 262b          	jrne	L52
 118                     ; 31 		TIM2_Cmd(DISABLE);
 120  0048 4f            	clr	a
 121  0049 cd0000        	call	_TIM2_Cmd
 123                     ; 32 		GPIO_WriteLow(GPIOC, GPIO_PIN_5);
 125  004c 4b20          	push	#32
 126  004e ae500a        	ldw	x,#20490
 127  0051 cd0000        	call	_GPIO_WriteLow
 129  0054 84            	pop	a
 130                     ; 33 		if(stopkymode == smSTOP)
 132  0055 b600          	ld	a,_stopkymode
 133  0057 a101          	cp	a,#1
 134  0059 2614          	jrne	L72
 135                     ; 35 			cas=0;
 137  005b ae0000        	ldw	x,#0
 138  005e bf02          	ldw	_cas+2,x
 139  0060 ae0000        	ldw	x,#0
 140  0063 bf00          	ldw	_cas,x
 141                     ; 36 			mezicas=0;
 143  0065 ae0000        	ldw	x,#0
 144  0068 bf06          	ldw	_mezicas+2,x
 145  006a ae0000        	ldw	x,#0
 146  006d bf04          	ldw	_mezicas,x
 147  006f               L72:
 148                     ; 38 		stopkymode = smSTOP;
 150  006f 35010000      	mov	_stopkymode,#1
 151  0073               L52:
 152                     ; 40 	if(GPIO_ReadInputPin(GPIOE, GPIO_PIN_0) == RESET) //tlacitko
 154  0073 4b01          	push	#1
 155  0075 ae5014        	ldw	x,#20500
 156  0078 cd0000        	call	_GPIO_ReadInputPin
 158  007b 5b01          	addw	sp,#1
 159  007d 4d            	tnz	a
 160  007e 2612          	jrne	L13
 161                     ; 42 		if (stopkymode != smSTOP)
 163  0080 b600          	ld	a,_stopkymode
 164  0082 a101          	cp	a,#1
 165  0084 270c          	jreq	L13
 166                     ; 44 			mezicas=cas;
 168  0086 be02          	ldw	x,_cas+2
 169  0088 bf06          	ldw	_mezicas+2,x
 170  008a be00          	ldw	x,_cas
 171  008c bf04          	ldw	_mezicas,x
 172                     ; 45 			stopkymode = smSPLIT;
 174  008e 35020000      	mov	_stopkymode,#2
 175  0092               L13:
 176                     ; 50 }
 179  0092 85            	popw	x
 180  0093 bf00          	ldw	c_y,x
 181  0095 320002        	pop	c_y+2
 182  0098 85            	popw	x
 183  0099 bf00          	ldw	c_x,x
 184  009b 320002        	pop	c_x+2
 185  009e 80            	iret
 210                     .const:	section	.text
 211  0000               L01:
 212  0000 00057e40      	dc.l	360000
 213                     ; 52 INTERRUPT_HANDLER(TIM2_UPD_OVF_BRK_IRQHandler, 13)
 213                     ; 53 {
 214                     	switch	.text
 215  009f               f_TIM2_UPD_OVF_BRK_IRQHandler:
 217  009f 8a            	push	cc
 218  00a0 84            	pop	a
 219  00a1 a4bf          	and	a,#191
 220  00a3 88            	push	a
 221  00a4 86            	pop	cc
 222  00a5 3b0002        	push	c_x+2
 223  00a8 be00          	ldw	x,c_x
 224  00aa 89            	pushw	x
 225  00ab 3b0002        	push	c_y+2
 226  00ae be00          	ldw	x,c_y
 227  00b0 89            	pushw	x
 228  00b1 be02          	ldw	x,c_lreg+2
 229  00b3 89            	pushw	x
 230  00b4 be00          	ldw	x,c_lreg
 231  00b6 89            	pushw	x
 234                     ; 54 	cas++;
 236  00b7 ae0000        	ldw	x,#_cas
 237  00ba a601          	ld	a,#1
 238  00bc cd0000        	call	c_lgadc
 240                     ; 55 	if (cas > 359999) {cas = 0;}
 242  00bf 9c            	rvf
 243  00c0 ae0000        	ldw	x,#_cas
 244  00c3 cd0000        	call	c_ltor
 246  00c6 ae0000        	ldw	x,#L01
 247  00c9 cd0000        	call	c_lcmp
 249  00cc 2f0a          	jrslt	L54
 252  00ce ae0000        	ldw	x,#0
 253  00d1 bf02          	ldw	_cas+2,x
 254  00d3 ae0000        	ldw	x,#0
 255  00d6 bf00          	ldw	_cas,x
 256  00d8               L54:
 257                     ; 56 	TIM2_ClearITPendingBit(TIM2_IT_UPDATE);
 259  00d8 a601          	ld	a,#1
 260  00da cd0000        	call	_TIM2_ClearITPendingBit
 262                     ; 57 }
 265  00dd 85            	popw	x
 266  00de bf00          	ldw	c_lreg,x
 267  00e0 85            	popw	x
 268  00e1 bf02          	ldw	c_lreg+2,x
 269  00e3 85            	popw	x
 270  00e4 bf00          	ldw	c_y,x
 271  00e6 320002        	pop	c_y+2
 272  00e9 85            	popw	x
 273  00ea bf00          	ldw	c_x,x
 274  00ec 320002        	pop	c_x+2
 275  00ef 80            	iret
 383                     ; 60 void main(void)
 383                     ; 61 {
 385                     	switch	.text
 386  00f0               _main:
 388  00f0 520b          	subw	sp,#11
 389       0000000b      OFST:	set	11
 392                     ; 65 		char zobrmisto=0;
 394  00f2 0f0b          	clr	(OFST+0,sp)
 396                     ; 66     char setin = 0;
 398                     ; 67     char sek = 0;
 400                     ; 68     char min = 0;
 402                     ; 71     CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);
 404  00f4 4f            	clr	a
 405  00f5 cd0000        	call	_CLK_HSIPrescalerConfig
 407                     ; 72 		GPIO_Init(GPIOC, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_SLOW);
 409  00f8 4bc0          	push	#192
 410  00fa 4b20          	push	#32
 411  00fc ae500a        	ldw	x,#20490
 412  00ff cd0000        	call	_GPIO_Init
 414  0102 85            	popw	x
 415                     ; 74 		GPIO_Init(GPIOD, GPIO_PIN_ALL, GPIO_MODE_OUT_PP_LOW_SLOW);
 417  0103 4bc0          	push	#192
 418  0105 4bff          	push	#255
 419  0107 ae500f        	ldw	x,#20495
 420  010a cd0000        	call	_GPIO_Init
 422  010d 85            	popw	x
 423                     ; 75     GPIO_Init(GPIOB, GPIO_PIN_ALL, GPIO_MODE_OUT_PP_HIGH_SLOW);
 425  010e 4bd0          	push	#208
 426  0110 4bff          	push	#255
 427  0112 ae5005        	ldw	x,#20485
 428  0115 cd0000        	call	_GPIO_Init
 430  0118 85            	popw	x
 431                     ; 77 		GPIO_Init(GPIOE, GPIO_PIN_0, GPIO_MODE_IN_PU_IT);
 433  0119 4b60          	push	#96
 434  011b 4b01          	push	#1
 435  011d ae5014        	ldw	x,#20500
 436  0120 cd0000        	call	_GPIO_Init
 438  0123 85            	popw	x
 439                     ; 78     GPIO_Init(GPIOE, GPIO_PIN_3, GPIO_MODE_IN_PU_IT);
 441  0124 4b60          	push	#96
 442  0126 4b08          	push	#8
 443  0128 ae5014        	ldw	x,#20500
 444  012b cd0000        	call	_GPIO_Init
 446  012e 85            	popw	x
 447                     ; 79 		GPIO_Init(GPIOE, GPIO_PIN_4, GPIO_MODE_IN_PU_IT);
 449  012f 4b60          	push	#96
 450  0131 4b10          	push	#16
 451  0133 ae5014        	ldw	x,#20500
 452  0136 cd0000        	call	_GPIO_Init
 454  0139 85            	popw	x
 455                     ; 80 		EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOE, EXTI_SENSITIVITY_FALL_ONLY);
 457  013a ae0402        	ldw	x,#1026
 458  013d cd0000        	call	_EXTI_SetExtIntSensitivity
 460                     ; 83 		TIM2_TimeBaseInit(TIM2_PRESCALER_128, 1249); //Nastaveno na 10 ms
 462  0140 ae04e1        	ldw	x,#1249
 463  0143 89            	pushw	x
 464  0144 a607          	ld	a,#7
 465  0146 cd0000        	call	_TIM2_TimeBaseInit
 467  0149 85            	popw	x
 468                     ; 84 		TIM2_ITConfig(TIM2_IT_UPDATE, ENABLE);
 470  014a ae0101        	ldw	x,#257
 471  014d cd0000        	call	_TIM2_ITConfig
 473                     ; 85 		TIM2_Cmd(DISABLE);
 475  0150 4f            	clr	a
 476  0151 cd0000        	call	_TIM2_Cmd
 478                     ; 87 		stopkymode = smSTOP;
 480  0154 35010000      	mov	_stopkymode,#1
 481                     ; 89 		enableInterrupts();
 484  0158 9a            rim
 486  0159               L121:
 487                     ; 93 			GPIO_Write(GPIOB, 0xFF);
 489  0159 4bff          	push	#255
 490  015b ae5005        	ldw	x,#20485
 491  015e cd0000        	call	_GPIO_Write
 493  0161 84            	pop	a
 494                     ; 94 			if (zobrmisto > 5)
 496  0162 7b0b          	ld	a,(OFST+0,sp)
 497  0164 a106          	cp	a,#6
 498  0166 2402          	jruge	L42
 499  0168 207e          	jp	L521
 500  016a               L42:
 501                     ; 96 				if (stopkymode == smSPLIT)
 503  016a b600          	ld	a,_stopkymode
 504  016c a102          	cp	a,#2
 505  016e 2606          	jrne	L721
 506                     ; 97 					{dZobrCas=mezicas;}
 508  0170 be06          	ldw	x,_mezicas+2
 509  0172 1f02          	ldw	(OFST-9,sp),x
 512  0174 2004          	jra	L131
 513  0176               L721:
 514                     ; 99 					{dZobrCas=cas;}
 516  0176 be02          	ldw	x,_cas+2
 517  0178 1f02          	ldw	(OFST-9,sp),x
 519  017a               L131:
 520                     ; 100 				setin= dZobrCas% 100;
 522  017a 1e02          	ldw	x,(OFST-9,sp)
 523  017c a664          	ld	a,#100
 524  017e cd0000        	call	c_smodx
 526  0181 01            	rrwa	x,a
 527  0182 6b01          	ld	(OFST-10,sp),a
 528  0184 02            	rlwa	x,a
 530                     ; 101 				PomCas = dZobrCas / 100;
 532  0185 1e02          	ldw	x,(OFST-9,sp)
 533  0187 a664          	ld	a,#100
 534  0189 cd0000        	call	c_sdivx
 536  018c 1f02          	ldw	(OFST-9,sp),x
 538                     ; 102 				sek = PomCas % 60;
 540  018e 1e02          	ldw	x,(OFST-9,sp)
 541  0190 a63c          	ld	a,#60
 542  0192 cd0000        	call	c_smodx
 544  0195 01            	rrwa	x,a
 545  0196 6b0b          	ld	(OFST+0,sp),a
 546  0198 02            	rlwa	x,a
 548                     ; 103 				min = PomCas / 60;
 550  0199 1e02          	ldw	x,(OFST-9,sp)
 551  019b a63c          	ld	a,#60
 552  019d cd0000        	call	c_sdivx
 554  01a0 01            	rrwa	x,a
 555  01a1 6b04          	ld	(OFST-7,sp),a
 556  01a3 02            	rlwa	x,a
 558                     ; 105 				PoleCas[5] = setin % 10;
 560  01a4 7b01          	ld	a,(OFST-10,sp)
 561  01a6 5f            	clrw	x
 562  01a7 97            	ld	xl,a
 563  01a8 a60a          	ld	a,#10
 564  01aa 62            	div	x,a
 565  01ab 5f            	clrw	x
 566  01ac 97            	ld	xl,a
 567  01ad 9f            	ld	a,xl
 568  01ae 6b0a          	ld	(OFST-1,sp),a
 570                     ; 106 				PoleCas[4] = setin / 10;
 572  01b0 7b01          	ld	a,(OFST-10,sp)
 573  01b2 5f            	clrw	x
 574  01b3 97            	ld	xl,a
 575  01b4 a60a          	ld	a,#10
 576  01b6 62            	div	x,a
 577  01b7 9f            	ld	a,xl
 578  01b8 6b09          	ld	(OFST-2,sp),a
 580                     ; 107 				PoleCas[3] = sek % 10;
 582  01ba 7b0b          	ld	a,(OFST+0,sp)
 583  01bc 5f            	clrw	x
 584  01bd 97            	ld	xl,a
 585  01be a60a          	ld	a,#10
 586  01c0 62            	div	x,a
 587  01c1 5f            	clrw	x
 588  01c2 97            	ld	xl,a
 589  01c3 9f            	ld	a,xl
 590  01c4 6b08          	ld	(OFST-3,sp),a
 592                     ; 108 				PoleCas[2] = sek / 10;
 594  01c6 7b0b          	ld	a,(OFST+0,sp)
 595  01c8 5f            	clrw	x
 596  01c9 97            	ld	xl,a
 597  01ca a60a          	ld	a,#10
 598  01cc 62            	div	x,a
 599  01cd 9f            	ld	a,xl
 600  01ce 6b07          	ld	(OFST-4,sp),a
 602                     ; 109 				PoleCas[1] = min % 10;
 604  01d0 7b04          	ld	a,(OFST-7,sp)
 605  01d2 5f            	clrw	x
 606  01d3 97            	ld	xl,a
 607  01d4 a60a          	ld	a,#10
 608  01d6 62            	div	x,a
 609  01d7 5f            	clrw	x
 610  01d8 97            	ld	xl,a
 611  01d9 9f            	ld	a,xl
 612  01da 6b06          	ld	(OFST-5,sp),a
 614                     ; 110 				PoleCas[0] = min / 10;
 616  01dc 7b04          	ld	a,(OFST-7,sp)
 617  01de 5f            	clrw	x
 618  01df 97            	ld	xl,a
 619  01e0 a60a          	ld	a,#10
 620  01e2 62            	div	x,a
 621  01e3 9f            	ld	a,xl
 622  01e4 6b05          	ld	(OFST-6,sp),a
 624                     ; 111 				zobrmisto=0;
 626  01e6 0f0b          	clr	(OFST+0,sp)
 628  01e8               L521:
 629                     ; 114 			if (zobrmisto == TECKA_POS)
 631  01e8 7b0b          	ld	a,(OFST+0,sp)
 632  01ea a103          	cp	a,#3
 633  01ec 2619          	jrne	L331
 634                     ; 115 				{ GPIO_Write(GPIOD, (PoleCas[zobrmisto] | TECKA_BIT));}
 636  01ee 96            	ldw	x,sp
 637  01ef 1c0005        	addw	x,#OFST-6
 638  01f2 9f            	ld	a,xl
 639  01f3 5e            	swapw	x
 640  01f4 1b0b          	add	a,(OFST+0,sp)
 641  01f6 2401          	jrnc	L41
 642  01f8 5c            	incw	x
 643  01f9               L41:
 644  01f9 02            	rlwa	x,a
 645  01fa f6            	ld	a,(x)
 646  01fb aa80          	or	a,#128
 647  01fd 88            	push	a
 648  01fe ae500f        	ldw	x,#20495
 649  0201 cd0000        	call	_GPIO_Write
 651  0204 84            	pop	a
 653  0205 2015          	jra	L531
 654  0207               L331:
 655                     ; 117 				{GPIO_Write(GPIOD, PoleCas[zobrmisto]);}
 657  0207 96            	ldw	x,sp
 658  0208 1c0005        	addw	x,#OFST-6
 659  020b 9f            	ld	a,xl
 660  020c 5e            	swapw	x
 661  020d 1b0b          	add	a,(OFST+0,sp)
 662  020f 2401          	jrnc	L61
 663  0211 5c            	incw	x
 664  0212               L61:
 665  0212 02            	rlwa	x,a
 666  0213 f6            	ld	a,(x)
 667  0214 88            	push	a
 668  0215 ae500f        	ldw	x,#20495
 669  0218 cd0000        	call	_GPIO_Write
 671  021b 84            	pop	a
 672  021c               L531:
 673                     ; 119 			GPIO_Write(GPIOB, ~(1<<zobrmisto));
 675  021c 7b0b          	ld	a,(OFST+0,sp)
 676  021e 5f            	clrw	x
 677  021f 97            	ld	xl,a
 678  0220 a601          	ld	a,#1
 679  0222 5d            	tnzw	x
 680  0223 2704          	jreq	L02
 681  0225               L22:
 682  0225 48            	sll	a
 683  0226 5a            	decw	x
 684  0227 26fc          	jrne	L22
 685  0229               L02:
 686  0229 43            	cpl	a
 687  022a 88            	push	a
 688  022b ae5005        	ldw	x,#20485
 689  022e cd0000        	call	_GPIO_Write
 691  0231 84            	pop	a
 692                     ; 122       zobrmisto++;
 694  0232 0c0b          	inc	(OFST+0,sp)
 696                     ; 123 			for( i=0;i < 200;i++);
 698  0234 0f04          	clr	(OFST-7,sp)
 700  0236               L731:
 704  0236 0c04          	inc	(OFST-7,sp)
 708  0238 7b04          	ld	a,(OFST-7,sp)
 709  023a a1c8          	cp	a,#200
 710  023c 25f8          	jrult	L731
 712  023e ac590159      	jpf	L121
 754                     	xdef	_main
 755                     	xdef	_mezicas
 756                     	xdef	_cas
 757                     	switch	.ubsct
 758  0000               _stopkymode:
 759  0000 00            	ds.b	1
 760                     	xdef	_stopkymode
 761                     	xdef	f_TIM2_UPD_OVF_BRK_IRQHandler
 762                     	xdef	f_EXTI_PORTE_IRQHandler
 763                     	xref	_TIM2_ClearITPendingBit
 764                     	xref	_TIM2_SetCounter
 765                     	xref	_TIM2_ITConfig
 766                     	xref	_TIM2_Cmd
 767                     	xref	_TIM2_TimeBaseInit
 768                     	xref	_GPIO_ReadInputPin
 769                     	xref	_GPIO_WriteLow
 770                     	xref	_GPIO_WriteHigh
 771                     	xref	_GPIO_Write
 772                     	xref	_GPIO_Init
 773                     	xref	_EXTI_SetExtIntSensitivity
 774                     	xref	_CLK_HSIPrescalerConfig
 775                     	xref.b	c_lreg
 776                     	xref.b	c_x
 777                     	xref.b	c_y
 797                     	xref	c_sdivx
 798                     	xref	c_smodx
 799                     	xref	c_lcmp
 800                     	xref	c_ltor
 801                     	xref	c_lgadc
 802                     	end
