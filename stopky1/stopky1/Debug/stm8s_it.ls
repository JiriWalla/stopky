   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.12.6 - 16 Dec 2021
   3                     ; Generator (Limited) V4.5.4 - 16 Dec 2021
  43                     ; 49 INTERRUPT_HANDLER(NonHandledInterrupt, 25)
  43                     ; 50 {
  44                     	switch	.text
  45  0000               f_NonHandledInterrupt:
  49                     ; 54 }
  52  0000 80            	iret
  74                     ; 62 INTERRUPT_HANDLER_TRAP(TRAP_IRQHandler)
  74                     ; 63 {
  75                     	switch	.text
  76  0001               f_TRAP_IRQHandler:
  80                     ; 67 }
  83  0001 80            	iret
 105                     ; 74 INTERRUPT_HANDLER(TLI_IRQHandler, 0)
 105                     ; 75 {
 106                     	switch	.text
 107  0002               f_TLI_IRQHandler:
 111                     ; 79 }
 114  0002 80            	iret
 136                     ; 86 INTERRUPT_HANDLER(AWU_IRQHandler, 1)
 136                     ; 87 {
 137                     	switch	.text
 138  0003               f_AWU_IRQHandler:
 142                     ; 91 }
 145  0003 80            	iret
 167                     ; 98 INTERRUPT_HANDLER(CLK_IRQHandler, 2)
 167                     ; 99 {
 168                     	switch	.text
 169  0004               f_CLK_IRQHandler:
 173                     ; 103 }
 176  0004 80            	iret
 199                     ; 110 INTERRUPT_HANDLER(EXTI_PORTA_IRQHandler, 3)
 199                     ; 111 {
 200                     	switch	.text
 201  0005               f_EXTI_PORTA_IRQHandler:
 205                     ; 115 }
 208  0005 80            	iret
 231                     ; 122 INTERRUPT_HANDLER(EXTI_PORTB_IRQHandler, 4)
 231                     ; 123 {
 232                     	switch	.text
 233  0006               f_EXTI_PORTB_IRQHandler:
 237                     ; 127 }
 240  0006 80            	iret
 263                     ; 134 INTERRUPT_HANDLER(EXTI_PORTC_IRQHandler, 5)
 263                     ; 135 {
 264                     	switch	.text
 265  0007               f_EXTI_PORTC_IRQHandler:
 269                     ; 139 }
 272  0007 80            	iret
 295                     ; 146 INTERRUPT_HANDLER(EXTI_PORTD_IRQHandler, 6)
 295                     ; 147 {
 296                     	switch	.text
 297  0008               f_EXTI_PORTD_IRQHandler:
 301                     ; 151 }
 304  0008 80            	iret
 327                     ; 158 INTERRUPT_HANDLER(EXTI_PORTE_IRQHandler, 7)
 327                     ; 159 {
 328                     	switch	.text
 329  0009               f_EXTI_PORTE_IRQHandler:
 333                     ; 163 }
 336  0009 80            	iret
 358                     ; 186 INTERRUPT_HANDLER(CAN_RX_IRQHandler, 8)
 358                     ; 187 {
 359                     	switch	.text
 360  000a               f_CAN_RX_IRQHandler:
 364                     ; 191 }
 367  000a 80            	iret
 389                     ; 198 INTERRUPT_HANDLER(CAN_TX_IRQHandler, 9)
 389                     ; 199 {
 390                     	switch	.text
 391  000b               f_CAN_TX_IRQHandler:
 395                     ; 203 }
 398  000b 80            	iret
 420                     ; 211 INTERRUPT_HANDLER(SPI_IRQHandler, 10)
 420                     ; 212 {
 421                     	switch	.text
 422  000c               f_SPI_IRQHandler:
 426                     ; 216 }
 429  000c 80            	iret
 452                     ; 223 INTERRUPT_HANDLER(TIM1_UPD_OVF_TRG_BRK_IRQHandler, 11)
 452                     ; 224 {
 453                     	switch	.text
 454  000d               f_TIM1_UPD_OVF_TRG_BRK_IRQHandler:
 458                     ; 228 }
 461  000d 80            	iret
 484                     ; 235 INTERRUPT_HANDLER(TIM1_CAP_COM_IRQHandler, 12)
 484                     ; 236 {
 485                     	switch	.text
 486  000e               f_TIM1_CAP_COM_IRQHandler:
 490                     ; 240 }
 493  000e 80            	iret
 516                     ; 287 INTERRUPT_HANDLER(TIM2_CAP_COM_IRQHandler, 14)
 516                     ; 288 {
 517                     	switch	.text
 518  000f               f_TIM2_CAP_COM_IRQHandler:
 522                     ; 292 }
 525  000f 80            	iret
 548                     ; 303 INTERRUPT_HANDLER(TIM3_UPD_OVF_BRK_IRQHandler, 15)
 548                     ; 304 {
 549                     	switch	.text
 550  0010               f_TIM3_UPD_OVF_BRK_IRQHandler:
 554                     ; 308 }
 557  0010 80            	iret
 580                     ; 315 INTERRUPT_HANDLER(TIM3_CAP_COM_IRQHandler, 16)
 580                     ; 316 {
 581                     	switch	.text
 582  0011               f_TIM3_CAP_COM_IRQHandler:
 586                     ; 320 }
 589  0011 80            	iret
 612                     ; 331 INTERRUPT_HANDLER(UART1_TX_IRQHandler, 17)
 612                     ; 332 {
 613                     	switch	.text
 614  0012               f_UART1_TX_IRQHandler:
 618                     ; 336 }
 621  0012 80            	iret
 644                     ; 343 INTERRUPT_HANDLER(UART1_RX_IRQHandler, 18)
 644                     ; 344 {
 645                     	switch	.text
 646  0013               f_UART1_RX_IRQHandler:
 650                     ; 348 }
 653  0013 80            	iret
 675                     ; 356 INTERRUPT_HANDLER(I2C_IRQHandler, 19)
 675                     ; 357 {
 676                     	switch	.text
 677  0014               f_I2C_IRQHandler:
 681                     ; 361 }
 684  0014 80            	iret
 707                     ; 397 INTERRUPT_HANDLER(UART3_TX_IRQHandler, 20)
 707                     ; 398 {
 708                     	switch	.text
 709  0015               f_UART3_TX_IRQHandler:
 713                     ; 402 }
 716  0015 80            	iret
 739                     ; 409 INTERRUPT_HANDLER(UART3_RX_IRQHandler, 21)
 739                     ; 410 {
 740                     	switch	.text
 741  0016               f_UART3_RX_IRQHandler:
 745                     ; 414 }
 748  0016 80            	iret
 770                     ; 424 INTERRUPT_HANDLER(ADC2_IRQHandler, 22)
 770                     ; 425 {
 771                     	switch	.text
 772  0017               f_ADC2_IRQHandler:
 776                     ; 429     return;
 779  0017 80            	iret
 802                     ; 467 INTERRUPT_HANDLER(TIM4_UPD_OVF_IRQHandler, 23)
 802                     ; 468 {
 803                     	switch	.text
 804  0018               f_TIM4_UPD_OVF_IRQHandler:
 808                     ; 472 }
 811  0018 80            	iret
 834                     ; 480 INTERRUPT_HANDLER(EEPROM_EEC_IRQHandler, 24)
 834                     ; 481 {
 835                     	switch	.text
 836  0019               f_EEPROM_EEC_IRQHandler:
 840                     ; 485 }
 843  0019 80            	iret
 855                     	xdef	f_EEPROM_EEC_IRQHandler
 856                     	xdef	f_TIM4_UPD_OVF_IRQHandler
 857                     	xdef	f_ADC2_IRQHandler
 858                     	xdef	f_UART3_TX_IRQHandler
 859                     	xdef	f_UART3_RX_IRQHandler
 860                     	xdef	f_I2C_IRQHandler
 861                     	xdef	f_UART1_RX_IRQHandler
 862                     	xdef	f_UART1_TX_IRQHandler
 863                     	xdef	f_TIM3_CAP_COM_IRQHandler
 864                     	xdef	f_TIM3_UPD_OVF_BRK_IRQHandler
 865                     	xdef	f_TIM2_CAP_COM_IRQHandler
 866                     	xdef	f_TIM1_UPD_OVF_TRG_BRK_IRQHandler
 867                     	xdef	f_TIM1_CAP_COM_IRQHandler
 868                     	xdef	f_SPI_IRQHandler
 869                     	xdef	f_CAN_TX_IRQHandler
 870                     	xdef	f_CAN_RX_IRQHandler
 871                     	xdef	f_EXTI_PORTE_IRQHandler
 872                     	xdef	f_EXTI_PORTD_IRQHandler
 873                     	xdef	f_EXTI_PORTC_IRQHandler
 874                     	xdef	f_EXTI_PORTB_IRQHandler
 875                     	xdef	f_EXTI_PORTA_IRQHandler
 876                     	xdef	f_CLK_IRQHandler
 877                     	xdef	f_AWU_IRQHandler
 878                     	xdef	f_TLI_IRQHandler
 879                     	xdef	f_TRAP_IRQHandler
 880                     	xdef	f_NonHandledInterrupt
 899                     	end
