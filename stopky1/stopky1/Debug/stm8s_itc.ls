   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.12.6 - 16 Dec 2021
   3                     ; Generator (Limited) V4.5.4 - 16 Dec 2021
  42                     ; 50 uint8_t ITC_GetCPUCC(void)
  42                     ; 51 {
  44                     	switch	.text
  45  0000               _ITC_GetCPUCC:
  49                     ; 53   _asm("push cc");
  52  0000 8a            push cc
  54                     ; 54   _asm("pop a");
  57  0001 84            pop a
  59                     ; 55   return; /* Ignore compiler warning, the returned value is in A register */
  62  0002 81            	ret
  85                     ; 83 void ITC_DeInit(void)
  85                     ; 84 {
  86                     	switch	.text
  87  0003               _ITC_DeInit:
  91                     ; 85   ITC->ISPR1 = ITC_SPRX_RESET_VALUE;
  93  0003 35ff7f70      	mov	32624,#255
  94                     ; 86   ITC->ISPR2 = ITC_SPRX_RESET_VALUE;
  96  0007 35ff7f71      	mov	32625,#255
  97                     ; 87   ITC->ISPR3 = ITC_SPRX_RESET_VALUE;
  99  000b 35ff7f72      	mov	32626,#255
 100                     ; 88   ITC->ISPR4 = ITC_SPRX_RESET_VALUE;
 102  000f 35ff7f73      	mov	32627,#255
 103                     ; 89   ITC->ISPR5 = ITC_SPRX_RESET_VALUE;
 105  0013 35ff7f74      	mov	32628,#255
 106                     ; 90   ITC->ISPR6 = ITC_SPRX_RESET_VALUE;
 108  0017 35ff7f75      	mov	32629,#255
 109                     ; 91   ITC->ISPR7 = ITC_SPRX_RESET_VALUE;
 111  001b 35ff7f76      	mov	32630,#255
 112                     ; 92   ITC->ISPR8 = ITC_SPRX_RESET_VALUE;
 114  001f 35ff7f77      	mov	32631,#255
 115                     ; 93 }
 118  0023 81            	ret
 143                     ; 100 uint8_t ITC_GetSoftIntStatus(void)
 143                     ; 101 {
 144                     	switch	.text
 145  0024               _ITC_GetSoftIntStatus:
 149                     ; 102   return (uint8_t)(ITC_GetCPUCC() & CPU_CC_I1I0);
 151  0024 adda          	call	_ITC_GetCPUCC
 153  0026 a428          	and	a,#40
 156  0028 81            	ret
 434                     .const:	section	.text
 435  0000               L62:
 436  0000 0065          	dc.w	L14
 437  0002 0065          	dc.w	L14
 438  0004 0065          	dc.w	L14
 439  0006 0065          	dc.w	L14
 440  0008 006e          	dc.w	L34
 441  000a 006e          	dc.w	L34
 442  000c 006e          	dc.w	L34
 443  000e 006e          	dc.w	L34
 444  0010 0077          	dc.w	L54
 445  0012 0077          	dc.w	L54
 446  0014 0077          	dc.w	L54
 447  0016 0077          	dc.w	L54
 448  0018 0080          	dc.w	L74
 449  001a 0080          	dc.w	L74
 450  001c 0080          	dc.w	L74
 451  001e 0080          	dc.w	L74
 452  0020 0089          	dc.w	L15
 453  0022 0089          	dc.w	L15
 454  0024 0089          	dc.w	L15
 455  0026 0089          	dc.w	L15
 456  0028 0092          	dc.w	L35
 457  002a 0092          	dc.w	L35
 458  002c 0092          	dc.w	L35
 459  002e 0092          	dc.w	L35
 460  0030 009b          	dc.w	L55
 461                     ; 110 ITC_PriorityLevel_TypeDef ITC_GetSoftwarePriority(ITC_Irq_TypeDef IrqNum)
 461                     ; 111 {
 462                     	switch	.text
 463  0029               _ITC_GetSoftwarePriority:
 465  0029 88            	push	a
 466  002a 89            	pushw	x
 467       00000002      OFST:	set	2
 470                     ; 112   uint8_t Value = 0;
 472  002b 0f02          	clr	(OFST+0,sp)
 474                     ; 113   uint8_t Mask = 0;
 476                     ; 116   assert_param(IS_ITC_IRQ_OK((uint8_t)IrqNum));
 478  002d a119          	cp	a,#25
 479  002f 2403          	jruge	L41
 480  0031 4f            	clr	a
 481  0032 2010          	jra	L61
 482  0034               L41:
 483  0034 ae0074        	ldw	x,#116
 484  0037 89            	pushw	x
 485  0038 ae0000        	ldw	x,#0
 486  003b 89            	pushw	x
 487  003c ae0064        	ldw	x,#L112
 488  003f cd0000        	call	_assert_failed
 490  0042 5b04          	addw	sp,#4
 491  0044               L61:
 492                     ; 119   Mask = (uint8_t)(0x03U << (((uint8_t)IrqNum % 4U) * 2U));
 494  0044 7b03          	ld	a,(OFST+1,sp)
 495  0046 a403          	and	a,#3
 496  0048 48            	sll	a
 497  0049 5f            	clrw	x
 498  004a 97            	ld	xl,a
 499  004b a603          	ld	a,#3
 500  004d 5d            	tnzw	x
 501  004e 2704          	jreq	L02
 502  0050               L22:
 503  0050 48            	sll	a
 504  0051 5a            	decw	x
 505  0052 26fc          	jrne	L22
 506  0054               L02:
 507  0054 6b01          	ld	(OFST-1,sp),a
 509                     ; 121   switch (IrqNum)
 511  0056 7b03          	ld	a,(OFST+1,sp)
 513                     ; 201   default:
 513                     ; 202     break;
 514  0058 a119          	cp	a,#25
 515  005a 2407          	jruge	L42
 516  005c 5f            	clrw	x
 517  005d 97            	ld	xl,a
 518  005e 58            	sllw	x
 519  005f de0000        	ldw	x,(L62,x)
 520  0062 fc            	jp	(x)
 521  0063               L42:
 522  0063 203d          	jra	L512
 523  0065               L14:
 524                     ; 123   case ITC_IRQ_TLI: /* TLI software priority can be read but has no meaning */
 524                     ; 124   case ITC_IRQ_AWU:
 524                     ; 125   case ITC_IRQ_CLK:
 524                     ; 126   case ITC_IRQ_PORTA:
 524                     ; 127     Value = (uint8_t)(ITC->ISPR1 & Mask); /* Read software priority */
 526  0065 c67f70        	ld	a,32624
 527  0068 1401          	and	a,(OFST-1,sp)
 528  006a 6b02          	ld	(OFST+0,sp),a
 530                     ; 128     break;
 532  006c 2034          	jra	L512
 533  006e               L34:
 534                     ; 130   case ITC_IRQ_PORTB:
 534                     ; 131   case ITC_IRQ_PORTC:
 534                     ; 132   case ITC_IRQ_PORTD:
 534                     ; 133   case ITC_IRQ_PORTE:
 534                     ; 134     Value = (uint8_t)(ITC->ISPR2 & Mask); /* Read software priority */
 536  006e c67f71        	ld	a,32625
 537  0071 1401          	and	a,(OFST-1,sp)
 538  0073 6b02          	ld	(OFST+0,sp),a
 540                     ; 135     break;
 542  0075 202b          	jra	L512
 543  0077               L54:
 544                     ; 138   case ITC_IRQ_CAN_RX:
 544                     ; 139   case ITC_IRQ_CAN_TX:
 544                     ; 140 #endif /*STM8S208 or STM8AF52Ax */
 544                     ; 141 #if defined(STM8S903) || defined(STM8AF622x)
 544                     ; 142   case ITC_IRQ_PORTF:
 544                     ; 143 #endif /*STM8S903 or STM8AF622x */
 544                     ; 144   case ITC_IRQ_SPI:
 544                     ; 145   case ITC_IRQ_TIM1_OVF:
 544                     ; 146     Value = (uint8_t)(ITC->ISPR3 & Mask); /* Read software priority */
 546  0077 c67f72        	ld	a,32626
 547  007a 1401          	and	a,(OFST-1,sp)
 548  007c 6b02          	ld	(OFST+0,sp),a
 550                     ; 147     break;
 552  007e 2022          	jra	L512
 553  0080               L74:
 554                     ; 149   case ITC_IRQ_TIM1_CAPCOM:
 554                     ; 150 #if defined (STM8S903) || defined (STM8AF622x)
 554                     ; 151   case ITC_IRQ_TIM5_OVFTRI:
 554                     ; 152   case ITC_IRQ_TIM5_CAPCOM:
 554                     ; 153 #else
 554                     ; 154   case ITC_IRQ_TIM2_OVF:
 554                     ; 155   case ITC_IRQ_TIM2_CAPCOM:
 554                     ; 156 #endif /* STM8S903 or STM8AF622x*/
 554                     ; 157   case ITC_IRQ_TIM3_OVF:
 554                     ; 158     Value = (uint8_t)(ITC->ISPR4 & Mask); /* Read software priority */
 556  0080 c67f73        	ld	a,32627
 557  0083 1401          	and	a,(OFST-1,sp)
 558  0085 6b02          	ld	(OFST+0,sp),a
 560                     ; 159     break;
 562  0087 2019          	jra	L512
 563  0089               L15:
 564                     ; 161   case ITC_IRQ_TIM3_CAPCOM:
 564                     ; 162 #if defined(STM8S208) ||defined(STM8S207) || defined (STM8S007) || defined(STM8S103) || \
 564                     ; 163     defined(STM8S003) ||defined(STM8S001) || defined (STM8S903) || defined (STM8AF52Ax) || defined (STM8AF62Ax)
 564                     ; 164   case ITC_IRQ_UART1_TX:
 564                     ; 165   case ITC_IRQ_UART1_RX:
 564                     ; 166 #endif /*STM8S208 or STM8S207 or STM8S007 or STM8S103 or STM8S003 or STM8S001 or STM8S903 or STM8AF52Ax or STM8AF62Ax */ 
 564                     ; 167 #if defined(STM8AF622x)
 564                     ; 168   case ITC_IRQ_UART4_TX:
 564                     ; 169   case ITC_IRQ_UART4_RX:
 564                     ; 170 #endif /*STM8AF622x */
 564                     ; 171   case ITC_IRQ_I2C:
 564                     ; 172     Value = (uint8_t)(ITC->ISPR5 & Mask); /* Read software priority */
 566  0089 c67f74        	ld	a,32628
 567  008c 1401          	and	a,(OFST-1,sp)
 568  008e 6b02          	ld	(OFST+0,sp),a
 570                     ; 173     break;
 572  0090 2010          	jra	L512
 573  0092               L35:
 574                     ; 181   case ITC_IRQ_UART3_TX:
 574                     ; 182   case ITC_IRQ_UART3_RX:
 574                     ; 183   case ITC_IRQ_ADC2:
 574                     ; 184 #endif /*STM8S208 or STM8S207 or STM8AF52Ax or STM8AF62Ax */
 574                     ; 185 #if defined(STM8S105) || defined(STM8S005) || defined(STM8S103) || defined(STM8S003) || \
 574                     ; 186     defined(STM8S001) || defined(STM8S903) || defined(STM8AF626x) || defined(STM8AF622x)
 574                     ; 187   case ITC_IRQ_ADC1:
 574                     ; 188 #endif /*STM8S105, STM8S005, STM8S103 or STM8S003 or STM8S001 or STM8S903 or STM8AF626x or STM8AF622x */
 574                     ; 189 #if defined (STM8S903) || defined (STM8AF622x)
 574                     ; 190   case ITC_IRQ_TIM6_OVFTRI:
 574                     ; 191 #else
 574                     ; 192   case ITC_IRQ_TIM4_OVF:
 574                     ; 193 #endif /*STM8S903 or STM8AF622x */
 574                     ; 194     Value = (uint8_t)(ITC->ISPR6 & Mask); /* Read software priority */
 576  0092 c67f75        	ld	a,32629
 577  0095 1401          	and	a,(OFST-1,sp)
 578  0097 6b02          	ld	(OFST+0,sp),a
 580                     ; 195     break;
 582  0099 2007          	jra	L512
 583  009b               L55:
 584                     ; 197   case ITC_IRQ_EEPROM_EEC:
 584                     ; 198     Value = (uint8_t)(ITC->ISPR7 & Mask); /* Read software priority */
 586  009b c67f76        	ld	a,32630
 587  009e 1401          	and	a,(OFST-1,sp)
 588  00a0 6b02          	ld	(OFST+0,sp),a
 590                     ; 199     break;
 592  00a2               L75:
 593                     ; 201   default:
 593                     ; 202     break;
 595  00a2               L512:
 596                     ; 205   Value >>= (uint8_t)(((uint8_t)IrqNum % 4u) * 2u);
 598  00a2 7b03          	ld	a,(OFST+1,sp)
 599  00a4 a403          	and	a,#3
 600  00a6 48            	sll	a
 601  00a7 5f            	clrw	x
 602  00a8 97            	ld	xl,a
 603  00a9 7b02          	ld	a,(OFST+0,sp)
 604  00ab 5d            	tnzw	x
 605  00ac 2704          	jreq	L03
 606  00ae               L23:
 607  00ae 44            	srl	a
 608  00af 5a            	decw	x
 609  00b0 26fc          	jrne	L23
 610  00b2               L03:
 611  00b2 6b02          	ld	(OFST+0,sp),a
 613                     ; 207   return((ITC_PriorityLevel_TypeDef)Value);
 615  00b4 7b02          	ld	a,(OFST+0,sp)
 618  00b6 5b03          	addw	sp,#3
 619  00b8 81            	ret
 685                     	switch	.const
 686  0032               L66:
 687  0032 014a          	dc.w	L712
 688  0034 014a          	dc.w	L712
 689  0036 014a          	dc.w	L712
 690  0038 014a          	dc.w	L712
 691  003a 015c          	dc.w	L122
 692  003c 015c          	dc.w	L122
 693  003e 015c          	dc.w	L122
 694  0040 015c          	dc.w	L122
 695  0042 016e          	dc.w	L322
 696  0044 016e          	dc.w	L322
 697  0046 016e          	dc.w	L322
 698  0048 016e          	dc.w	L322
 699  004a 0180          	dc.w	L522
 700  004c 0180          	dc.w	L522
 701  004e 0180          	dc.w	L522
 702  0050 0180          	dc.w	L522
 703  0052 0192          	dc.w	L722
 704  0054 0192          	dc.w	L722
 705  0056 0192          	dc.w	L722
 706  0058 0192          	dc.w	L722
 707  005a 01a4          	dc.w	L132
 708  005c 01a4          	dc.w	L132
 709  005e 01a4          	dc.w	L132
 710  0060 01a4          	dc.w	L132
 711  0062 01b6          	dc.w	L332
 712                     ; 223 void ITC_SetSoftwarePriority(ITC_Irq_TypeDef IrqNum, ITC_PriorityLevel_TypeDef PriorityValue)
 712                     ; 224 {
 713                     	switch	.text
 714  00b9               _ITC_SetSoftwarePriority:
 716  00b9 89            	pushw	x
 717  00ba 89            	pushw	x
 718       00000002      OFST:	set	2
 721                     ; 225   uint8_t Mask = 0;
 723                     ; 226   uint8_t NewPriority = 0;
 725                     ; 229   assert_param(IS_ITC_IRQ_OK((uint8_t)IrqNum));
 727  00bb 9e            	ld	a,xh
 728  00bc a119          	cp	a,#25
 729  00be 2403          	jruge	L63
 730  00c0 4f            	clr	a
 731  00c1 2010          	jra	L04
 732  00c3               L63:
 733  00c3 ae00e5        	ldw	x,#229
 734  00c6 89            	pushw	x
 735  00c7 ae0000        	ldw	x,#0
 736  00ca 89            	pushw	x
 737  00cb ae0064        	ldw	x,#L112
 738  00ce cd0000        	call	_assert_failed
 740  00d1 5b04          	addw	sp,#4
 741  00d3               L04:
 742                     ; 230   assert_param(IS_ITC_PRIORITY_OK(PriorityValue));
 744  00d3 7b04          	ld	a,(OFST+2,sp)
 745  00d5 a102          	cp	a,#2
 746  00d7 2710          	jreq	L44
 747  00d9 7b04          	ld	a,(OFST+2,sp)
 748  00db a101          	cp	a,#1
 749  00dd 270a          	jreq	L44
 750  00df 0d04          	tnz	(OFST+2,sp)
 751  00e1 2706          	jreq	L44
 752  00e3 7b04          	ld	a,(OFST+2,sp)
 753  00e5 a103          	cp	a,#3
 754  00e7 2603          	jrne	L24
 755  00e9               L44:
 756  00e9 4f            	clr	a
 757  00ea 2010          	jra	L64
 758  00ec               L24:
 759  00ec ae00e6        	ldw	x,#230
 760  00ef 89            	pushw	x
 761  00f0 ae0000        	ldw	x,#0
 762  00f3 89            	pushw	x
 763  00f4 ae0064        	ldw	x,#L112
 764  00f7 cd0000        	call	_assert_failed
 766  00fa 5b04          	addw	sp,#4
 767  00fc               L64:
 768                     ; 233   assert_param(IS_ITC_INTERRUPTS_DISABLED);
 770  00fc cd0024        	call	_ITC_GetSoftIntStatus
 772  00ff a128          	cp	a,#40
 773  0101 2603          	jrne	L05
 774  0103 4f            	clr	a
 775  0104 2010          	jra	L25
 776  0106               L05:
 777  0106 ae00e9        	ldw	x,#233
 778  0109 89            	pushw	x
 779  010a ae0000        	ldw	x,#0
 780  010d 89            	pushw	x
 781  010e ae0064        	ldw	x,#L112
 782  0111 cd0000        	call	_assert_failed
 784  0114 5b04          	addw	sp,#4
 785  0116               L25:
 786                     ; 237   Mask = (uint8_t)(~(uint8_t)(0x03U << (((uint8_t)IrqNum % 4U) * 2U)));
 788  0116 7b03          	ld	a,(OFST+1,sp)
 789  0118 a403          	and	a,#3
 790  011a 48            	sll	a
 791  011b 5f            	clrw	x
 792  011c 97            	ld	xl,a
 793  011d a603          	ld	a,#3
 794  011f 5d            	tnzw	x
 795  0120 2704          	jreq	L45
 796  0122               L65:
 797  0122 48            	sll	a
 798  0123 5a            	decw	x
 799  0124 26fc          	jrne	L65
 800  0126               L45:
 801  0126 43            	cpl	a
 802  0127 6b01          	ld	(OFST-1,sp),a
 804                     ; 240   NewPriority = (uint8_t)((uint8_t)(PriorityValue) << (((uint8_t)IrqNum % 4U) * 2U));
 806  0129 7b03          	ld	a,(OFST+1,sp)
 807  012b a403          	and	a,#3
 808  012d 48            	sll	a
 809  012e 5f            	clrw	x
 810  012f 97            	ld	xl,a
 811  0130 7b04          	ld	a,(OFST+2,sp)
 812  0132 5d            	tnzw	x
 813  0133 2704          	jreq	L06
 814  0135               L26:
 815  0135 48            	sll	a
 816  0136 5a            	decw	x
 817  0137 26fc          	jrne	L26
 818  0139               L06:
 819  0139 6b02          	ld	(OFST+0,sp),a
 821                     ; 242   switch (IrqNum)
 823  013b 7b03          	ld	a,(OFST+1,sp)
 825                     ; 332   default:
 825                     ; 333     break;
 826  013d a119          	cp	a,#25
 827  013f 2407          	jruge	L46
 828  0141 5f            	clrw	x
 829  0142 97            	ld	xl,a
 830  0143 58            	sllw	x
 831  0144 de0032        	ldw	x,(L66,x)
 832  0147 fc            	jp	(x)
 833  0148               L46:
 834  0148 207c          	jra	L372
 835  014a               L712:
 836                     ; 244   case ITC_IRQ_TLI: /* TLI software priority can be written but has no meaning */
 836                     ; 245   case ITC_IRQ_AWU:
 836                     ; 246   case ITC_IRQ_CLK:
 836                     ; 247   case ITC_IRQ_PORTA:
 836                     ; 248     ITC->ISPR1 &= Mask;
 838  014a c67f70        	ld	a,32624
 839  014d 1401          	and	a,(OFST-1,sp)
 840  014f c77f70        	ld	32624,a
 841                     ; 249     ITC->ISPR1 |= NewPriority;
 843  0152 c67f70        	ld	a,32624
 844  0155 1a02          	or	a,(OFST+0,sp)
 845  0157 c77f70        	ld	32624,a
 846                     ; 250     break;
 848  015a 206a          	jra	L372
 849  015c               L122:
 850                     ; 252   case ITC_IRQ_PORTB:
 850                     ; 253   case ITC_IRQ_PORTC:
 850                     ; 254   case ITC_IRQ_PORTD:
 850                     ; 255   case ITC_IRQ_PORTE:
 850                     ; 256     ITC->ISPR2 &= Mask;
 852  015c c67f71        	ld	a,32625
 853  015f 1401          	and	a,(OFST-1,sp)
 854  0161 c77f71        	ld	32625,a
 855                     ; 257     ITC->ISPR2 |= NewPriority;
 857  0164 c67f71        	ld	a,32625
 858  0167 1a02          	or	a,(OFST+0,sp)
 859  0169 c77f71        	ld	32625,a
 860                     ; 258     break;
 862  016c 2058          	jra	L372
 863  016e               L322:
 864                     ; 261   case ITC_IRQ_CAN_RX:
 864                     ; 262   case ITC_IRQ_CAN_TX:
 864                     ; 263 #endif /*STM8S208 or STM8AF52Ax */
 864                     ; 264 #if defined(STM8S903) || defined(STM8AF622x)
 864                     ; 265   case ITC_IRQ_PORTF:
 864                     ; 266 #endif /*STM8S903 or STM8AF622x */
 864                     ; 267   case ITC_IRQ_SPI:
 864                     ; 268   case ITC_IRQ_TIM1_OVF:
 864                     ; 269     ITC->ISPR3 &= Mask;
 866  016e c67f72        	ld	a,32626
 867  0171 1401          	and	a,(OFST-1,sp)
 868  0173 c77f72        	ld	32626,a
 869                     ; 270     ITC->ISPR3 |= NewPriority;
 871  0176 c67f72        	ld	a,32626
 872  0179 1a02          	or	a,(OFST+0,sp)
 873  017b c77f72        	ld	32626,a
 874                     ; 271     break;
 876  017e 2046          	jra	L372
 877  0180               L522:
 878                     ; 273   case ITC_IRQ_TIM1_CAPCOM:
 878                     ; 274 #if defined(STM8S903) || defined(STM8AF622x) 
 878                     ; 275   case ITC_IRQ_TIM5_OVFTRI:
 878                     ; 276   case ITC_IRQ_TIM5_CAPCOM:
 878                     ; 277 #else
 878                     ; 278   case ITC_IRQ_TIM2_OVF:
 878                     ; 279   case ITC_IRQ_TIM2_CAPCOM:
 878                     ; 280 #endif /*STM8S903 or STM8AF622x */
 878                     ; 281   case ITC_IRQ_TIM3_OVF:
 878                     ; 282     ITC->ISPR4 &= Mask;
 880  0180 c67f73        	ld	a,32627
 881  0183 1401          	and	a,(OFST-1,sp)
 882  0185 c77f73        	ld	32627,a
 883                     ; 283     ITC->ISPR4 |= NewPriority;
 885  0188 c67f73        	ld	a,32627
 886  018b 1a02          	or	a,(OFST+0,sp)
 887  018d c77f73        	ld	32627,a
 888                     ; 284     break;
 890  0190 2034          	jra	L372
 891  0192               L722:
 892                     ; 286   case ITC_IRQ_TIM3_CAPCOM:
 892                     ; 287 #if defined(STM8S208) ||defined(STM8S207) || defined (STM8S007) || defined(STM8S103) || \
 892                     ; 288     defined(STM8S001) ||defined(STM8S003) ||defined(STM8S903) || defined (STM8AF52Ax) || defined (STM8AF62Ax)
 892                     ; 289   case ITC_IRQ_UART1_TX:
 892                     ; 290   case ITC_IRQ_UART1_RX:
 892                     ; 291 #endif /*STM8S208 or STM8S207 or STM8S007 or STM8S103 or STM8S003 or STM8S001 or STM8S903 or STM8AF52Ax or STM8AF62Ax */ 
 892                     ; 292 #if defined(STM8AF622x)
 892                     ; 293   case ITC_IRQ_UART4_TX:
 892                     ; 294   case ITC_IRQ_UART4_RX:
 892                     ; 295 #endif /*STM8AF622x */
 892                     ; 296   case ITC_IRQ_I2C:
 892                     ; 297     ITC->ISPR5 &= Mask;
 894  0192 c67f74        	ld	a,32628
 895  0195 1401          	and	a,(OFST-1,sp)
 896  0197 c77f74        	ld	32628,a
 897                     ; 298     ITC->ISPR5 |= NewPriority;
 899  019a c67f74        	ld	a,32628
 900  019d 1a02          	or	a,(OFST+0,sp)
 901  019f c77f74        	ld	32628,a
 902                     ; 299     break;
 904  01a2 2022          	jra	L372
 905  01a4               L132:
 906                     ; 308   case ITC_IRQ_UART3_TX:
 906                     ; 309   case ITC_IRQ_UART3_RX:
 906                     ; 310   case ITC_IRQ_ADC2:
 906                     ; 311 #endif /*STM8S208 or STM8S207 or STM8AF52Ax or STM8AF62Ax */
 906                     ; 312     
 906                     ; 313 #if defined(STM8S105) || defined(STM8S005) || defined(STM8S103) || defined(STM8S003) || \
 906                     ; 314     defined(STM8S001) || defined(STM8S903) || defined(STM8AF626x) || defined (STM8AF622x)
 906                     ; 315   case ITC_IRQ_ADC1:
 906                     ; 316 #endif /*STM8S105, STM8S005, STM8S103 or STM8S003 or STM8S001 or STM8S903 or STM8AF626x or STM8AF622x */
 906                     ; 317     
 906                     ; 318 #if defined (STM8S903) || defined (STM8AF622x)
 906                     ; 319   case ITC_IRQ_TIM6_OVFTRI:
 906                     ; 320 #else
 906                     ; 321   case ITC_IRQ_TIM4_OVF:
 906                     ; 322 #endif /* STM8S903 or STM8AF622x */
 906                     ; 323     ITC->ISPR6 &= Mask;
 908  01a4 c67f75        	ld	a,32629
 909  01a7 1401          	and	a,(OFST-1,sp)
 910  01a9 c77f75        	ld	32629,a
 911                     ; 324     ITC->ISPR6 |= NewPriority;
 913  01ac c67f75        	ld	a,32629
 914  01af 1a02          	or	a,(OFST+0,sp)
 915  01b1 c77f75        	ld	32629,a
 916                     ; 325     break;
 918  01b4 2010          	jra	L372
 919  01b6               L332:
 920                     ; 327   case ITC_IRQ_EEPROM_EEC:
 920                     ; 328     ITC->ISPR7 &= Mask;
 922  01b6 c67f76        	ld	a,32630
 923  01b9 1401          	and	a,(OFST-1,sp)
 924  01bb c77f76        	ld	32630,a
 925                     ; 329     ITC->ISPR7 |= NewPriority;
 927  01be c67f76        	ld	a,32630
 928  01c1 1a02          	or	a,(OFST+0,sp)
 929  01c3 c77f76        	ld	32630,a
 930                     ; 330     break;
 932  01c6               L532:
 933                     ; 332   default:
 933                     ; 333     break;
 935  01c6               L372:
 936                     ; 335 }
 939  01c6 5b04          	addw	sp,#4
 940  01c8 81            	ret
 953                     	xdef	_ITC_GetSoftwarePriority
 954                     	xdef	_ITC_SetSoftwarePriority
 955                     	xdef	_ITC_GetSoftIntStatus
 956                     	xdef	_ITC_DeInit
 957                     	xdef	_ITC_GetCPUCC
 958                     	xref	_assert_failed
 959                     	switch	.const
 960  0064               L112:
 961  0064 7372635c7374  	dc.b	"src\stm8s_itc.c",0
 981                     	end
